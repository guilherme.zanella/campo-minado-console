package cm;

import cm.modelo.Tabuleiro;
import cm.visao.TabuleiroConsole;

public class Aplicacao {
    public static void main(String[] args) {
        Tabuleiro tab = new Tabuleiro(6, 6, 6);
        new TabuleiroConsole(tab);
    }
}

